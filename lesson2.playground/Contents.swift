import UIKit

func showString() {
    print("It's a string")
}

showString()
print("-------------------------")


func summOfTwoNumber(firstNumber: Int, secondNumber: Int) {
    
    let summ = firstNumber + secondNumber
    print(summ)
}

summOfTwoNumber(firstNumber: 3, secondNumber: 5)
print("-------------------------")

func summOfTwoNumberOption2(firstNumber a: Int = 1, secondNumber b: Int) {
    let summ = a + b
    print(summ)
}

summOfTwoNumberOption2(firstNumber: 5, secondNumber: 20)
print("-------------------------")

var summOfUsingFunctionDiv = 0

func arithmeticDiv(firstValue a: Double, secondValue b: Double) {
    
    let resultDiv = a / b
    summOfUsingFunctionDiv += 1
    print(resultDiv)
}

arithmeticDiv(firstValue: 10, secondValue: 3)
arithmeticDiv(firstValue: 20, secondValue: 4)
print(summOfUsingFunctionDiv)
print("-------------------------")

func getSummOfWords(firstNumber a: String, secondNumber b: String) -> String {
    
    let summOfWord = a + b
    return summOfWord
}

let summ = getSummOfWords(firstNumber: "30", secondNumber: "50")
print(summ)

func getFullStringByWords(firstWord a: String, secondWord b: String) -> String {
    
    let summ = a + b
    return summ
}

let fullString = getFullStringByWords(firstWord: "hi", secondWord: "pedro")
print(fullString)
print("-------------------------")

func vari<N>(members: N...) {
    
    print(members.count)
   for i in members {
      print(i)
   }
}

vari(members: "qwe", "123")
vari(members: 1500)
vari(members: 13.5)



var array: [String] = ["qwe", "ww"]

for i in array {
    print(i)
}

for i in array.indices {
    print(i)
}

array.forEach({ item in
    print(item)
})

array.forEach({
    print($0)
})

array.last
array[1]
array.append("123")

//print(array)

var array2 = ["qweqwe"]
print(array, array2)


let tuple = (123, "qwe", 5, 3.4)
print(tuple)

var arrayFunction = [5, 3, 4, 2]
let arrayStrings = ["qweqw", "www"]
let result = arrayFunction.sorted(by: {$0 > $1})
let summOfArrayValues = arrayStrings.map({$0.count})

var arrayFunctionWithNil = [5, 3, 4, nil]

let arrayOfStrings = ["qweqweq", "wwweqwe", "eeeeeee"]
let arrayWithOutNil = arrayOfStrings.flatMap({$0})

let arrayOfString = ["1", "3", "5", "qweqwe", "rret", "qqq"]
print(arrayOfString)

var arrayOfInts = [1, 3, 4, 4]
let filteredData = arrayOfInts.filter({$0 <= 3})
print(filteredData)
let filteredArrayString = arrayOfString.filter({$0.contains("q")})
print(filteredArrayString)

let summOfByReduce = arrayOfInts.reduce(0, +)
print(summOfByReduce)

let maxValue = arrayOfInts.min()
print(maxValue)


